const express= require ('express');
const path = require('path');
const formController = require('./controllers/form-controller');
const bodyParser = require ('body-parser');
const app = express();


app.set('view engine', 'ejs');
app.use(express.static (path.join(__dirname, '/public')));
app.use(bodyParser.urlencoded());


const handlerRaiz = (req,res,next)=>{
    res.render('index');
};

app.get('/form', formController.formGet);
app.get('/', handlerRaiz);
app.get('/sobre', (req,res,next)=>{
    res.render('sobre');
});

app.post('/form', formController.formPost);

app.listen(3001, () =>{
    console.log('Servido On');
});

