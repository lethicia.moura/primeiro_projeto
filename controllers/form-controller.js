const fs = require('fs');
const ejs = require ('ejs');
const htmlPDF = require('html-pdf-node');

const handlerGetForm = (req,res) =>{
    res.render('form');
};

const handlerPostForm = (req,res) =>{
   const body = req.body;
   const viewModel={
       Nome: body.Nome,
       Email: body.Email,
       DatadeNascimento: body.DatadeNascimento,
       Telefone: body.Telefone
   }
 const htmltext = fs.readFileSync('./views/form-pdf.ejs', 'utf-8');
 const htmlPronto = ejs.render(htmltext, viewModel);

 let file = {content: htmlPronto}
 let options = {format:'A4'}
 
 htmlPDF.generatePdf(file, options).then(pdfbuffer =>{
     res.contentType('application/pdf');
     res.send(pdfbuffer);
 });

};

module.exports={
    formGet: handlerGetForm,
    formPost: handlerPostForm
};